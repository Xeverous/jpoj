package core;

import java.awt.Color;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import creatures.*;
import gui.ColorGrid;

public class World
{
	public World()
	{
		board.ensureCapacity(boardSize);
		for (int y = 0; y < boardSize; ++y)
		{
			board.add(new ArrayList<Organism>());
			board.get(y).ensureCapacity(boardSize);

			for (int x = 0; x < boardSize; ++x)
				board.get(y).add(null);
		}
	}

	public void drawOn(ColorGrid colorGrid)
	{
		for (int y = 0; y < board.size(); ++y)
			for (int x = 0; x < board.get(y).size(); ++x)
			{
				Organism organism = board.get(y).get(x);

				if (organism == null)
					colorGrid.setColor(x, y, Color.white);
				else
					colorGrid.setColor(x, y, organism.getColor());
			}
	}

	public void processTurn() throws Exception
	{
		++turnCount;

		ArrayList<Organism> organisms = new ArrayList<Organism>();

		for (int y = 0; y < board.size(); ++y)
			for (int x = 0; x < board.get(y).size(); ++x)
			{
				Organism ptr = board.get(y).get(x);

				if (ptr != null)
				{
					organisms.add(ptr);
					map.put(ptr, new Point(x, y));
				}
			}

		Collections.sort(organisms, new Comparator<Organism>()
		{
			@Override
			public int compare(Organism left, Organism right)
			{
				if (left.getInitiative() == right.getInitiative())
					return left.getAge() - right.getAge();
				else
					return left.getInitiative() - right.getInitiative();
			}
		});

		for (Organism organism : organisms)
			organism.doAge();

		for (Organism organism : organisms)
			if (organism.isAlive()) // some things can die before their's turn
				organism.action(map.get(organism));

		for (ArrayList<Organism> row : board)
			for (int i = 0; i < row.size(); ++i)
				if (row.get(i) != null && row.get(i).isDead())
					row.set(i, null);

		map.clear();
	}

	/**
	 * @brief moves organism from one position to another
	 * @details if both positions are occupied, organisms are swapped
	 * @param old_position current position of the organism, must be occupied
	 * @param new_position position to move to
	 */
	public void moveOrganism(Point oldPosition, Point newPosition)
	{
		Organism current = board.get(oldPosition.y).get(oldPosition.x);
		Organism target  = board.get(newPosition.y).get(newPosition.x);

		map.put(current, new Point(newPosition));

		if (target != null)
			map.put(target, new Point(oldPosition));

		// swap organisms
		Organism temp = current;
		current = target;
		target = temp;
	}

	public boolean isValid(Point position)
	{
		return
			position.x < getBoardSize().x &&
			position.y < getBoardSize().y &&
			position.y >= 0               &&
			position.x >= 0;
	}

	public boolean isFree(Point position)
	{
		Organism organism = board.get(position.y).get(position.x);

		if (organism == null)
			return true;
		else
			return organism.isDead();
	}

	public static Point getBoardSize()
	{
		return new Point(boardSize, boardSize);
	}

	private enum Direction
	{
		up, down, left, right;

		public static Direction fromInt(int i)
		{
			return Direction.values()[i];
		}
	}

	public Point generateAdjacentPosition(Point position)
	{
		Supplier<Point> generate = () ->
		{
			int randomDirection = ThreadLocalRandom.current().nextInt(0, Direction.values().length);
			Direction direction = Direction.fromInt(randomDirection);

			if (direction == Direction.up)
				return new Point(position.x,     position.y - 1);
			else if (direction == Direction.down)
				return new Point(position.x,     position.y + 1);
			else if (direction == Direction.left)
				return new Point(position.x - 1, position.y    );
			else // if (direction == Direction.right)
				return new Point(position.x + 1, position.y    );
		};

		Point result;

		do
		{
			result = generate.get();
		} while (!isValid(result));

		return result;
	}

	public Point generateRandomFreePosition() throws Exception
	{
		ArrayList<Point> emptyPositions = new ArrayList<>();

		for (int y = 0; y < board.size(); ++y)
			for (int x = 0; x < board.get(y).size(); ++x)
			{
				Point p = new Point(x, y);

				if (isFree(p))
					emptyPositions.add(p);
			}

		if (emptyPositions.isEmpty())
			throw new Exception("no empty positions on the board");

		int randomPosition = ThreadLocalRandom.current().nextInt(0, emptyPositions.size());
		return emptyPositions.get(randomPosition);
	}

	/**
	 * @brief may return null
	 */
	public Organism getOrganism(Point position)
	{
		return board.get(position.y).get(position.x);
	}

	public Point getPosition(Organism organism)
	{
		return map.get(organism);
	}

	/**
	 * @brief Adds adjacent positions based on the given origin
	 * @details Function adds up to 4 (only valid) positions
	 * @param origin position to which add/subtract 1 on x/y axis
	 * @param positions vector to fill with results
	 */
	public void appendAdjacentPositions(Point origin, ArrayList<Point> positions)
	{
		Consumer<Point> attempt = (Point position) ->
		{
			if (isValid(position))
				positions.add(position);
		};

		attempt.accept(new Point(origin.x - 1, origin.y    ));
		attempt.accept(new Point(origin.x + 1, origin.y    ));
		attempt.accept(new Point(origin.x,     origin.y - 1));
		attempt.accept(new Point(origin.x,     origin.y + 1));
	}

	public static void removeDuplicatePositions(ArrayList<Point> positions)
	{
		Set<Point> hs = new HashSet<Point>();
		hs.addAll(positions);
		positions.clear();
		positions.addAll(hs);
	}

	public void removeOccupiedPositions(ArrayList<Point> positions)
	{
		positions.removeIf((Point p) -> { return !isFree(p); });
	}

	public boolean placeNewOrganismFromOneParent(Point parent, Organism organism)
	{
		ArrayList<Point> validPositions = new ArrayList<>();
		appendAdjacentPositions(parent, validPositions);
		removeOccupiedPositions(validPositions);

		if (validPositions.isEmpty())
			return false;

		placeNewOrganism(validPositions, organism);
		return true;
	}

	public boolean placeNewOrganismFromTwoParents(Point parent1, Point parent2, Organism organism)
	{
		ArrayList<Point> validPositions = new ArrayList<>();

		appendAdjacentPositions(parent1, validPositions);
		appendAdjacentPositions(parent2, validPositions);

		removeDuplicatePositions(validPositions);
		removeOccupiedPositions(validPositions);

		if (validPositions.isEmpty())
			return false;

		placeNewOrganism(validPositions, organism);
		return true;
	}

	private void placeNewOrganism(ArrayList<Point> possiblePositions, Organism organism)
	{
		int randomIndex = ThreadLocalRandom.current().nextInt(0, possiblePositions.size());
		Point position = possiblePositions.get(randomIndex);
		placeNewOrganism(position, organism);
	}

	private void placeNewOrganism(Point position, Organism organism)
	{
		board.get(position.y).set(position.x, organism);
		map.put(organism, position);
	}

	public void save() throws IOException
	{
		FileWriter fw = new FileWriter(saveFileName);

		for (ArrayList<Organism> row : board)
			for (Organism organism : row)
				if (organism == null)
				{
					fw.write(Organism.emptyFieldToken);
					fw.write(Organism.valueDelimeter);
					fw.write('\n');
				}
				else
				{
					organism.save(fw);
				}

		fw.write(String.valueOf(turnCount));
		fw.write(Organism.valueDelimeter);
		fw.write('\n');
		fw.close();
	}

	public void load() throws IOException
	{
		try (BufferedReader br = new BufferedReader(new FileReader(saveFileName)))
		{
			String line;
			int index = 0;
			final int fields = getBoardSize().x * getBoardSize().y;

			Function<Integer, Organism> getCurrentField = (Integer i) ->
			{
				return board.get(i / getBoardSize().x).get(i % getBoardSize().x);
			};

			for (; (line = br.readLine()) != null; ++index)
			{
				if (index == fields)
					break;

				String[] tokens = line.split(String.valueOf(Organism.valueDelimeter));

				if (tokens.length == 1 && tokens[0].equals(Organism.emptyFieldToken))
				{
					board.get(index / getBoardSize().x).set(index % getBoardSize().x, null);
					continue;
				}

				if (tokens.length != Organism.tokensPerLine && tokens.length != 1)
					throw new RuntimeException("parse error: line " + String.valueOf(index + 1) + " with contents:\n" + line);

				String type    = tokens[0];
				int strength   = Integer.parseInt(tokens[1]);
				int initiative = Integer.parseInt(tokens[2]);
				int age        = Integer.parseInt(tokens[3]);

				boolean validOrganismType = false;
				for (OrganismType organismType : OrganismType.values())
					if (organismType.toString().equals(type))
					{
						createNewOrganism(new Point(index % getBoardSize().x, index / getBoardSize().x), organismType);
						validOrganismType = true;
						break;
					}

				if (validOrganismType)
				{
					getCurrentField.apply(index).setStrength(strength);
					getCurrentField.apply(index).setInitiative(initiative);
					getCurrentField.apply(index).setAge(age);
				}
				else
				{
					throw new RuntimeException("parsing error: invalid organism type: " + type);
				}
			}

			if (index < fields)
				throw new RuntimeException("parsing error: save file too short - only " + String.valueOf(index)
					+ " lines out of " + String.valueOf(fields));

			String[] tokens = line.split(String.valueOf(Organism.valueDelimeter));
			turnCount = Integer.parseInt(tokens[0]);
		}
	}

	public void generateRandomWorld() throws Exception
	{
		ArrayList<Point> allPositions = new ArrayList<>();
		allPositions.ensureCapacity(getBoardSize().x * getBoardSize().y);

		for (int y = 0; y < board.size(); ++y)
			for (int x = 0; x < board.get(y).size(); ++x)
			{
				allPositions.add(new Point(x, y));
				board.get(y).set(x, null);
			}

		ArrayList<OrganismType> organismsToCreate = new ArrayList<>();

		for (OrganismType organismType : OrganismType.values())
			for (int i = 0; i < 5; ++i)
				organismsToCreate.add(organismType);

		if (allPositions.size() < organismsToCreate.size())
			throw new Exception("too many organisms to create");

		Collections.shuffle(allPositions);

		for (int i = 0; i < organismsToCreate.size(); ++i)
			createNewOrganism(allPositions.get(i), organismsToCreate.get(i));
	}

	/**
	 * @brief spawns a new organism with default Organism ctor
	 */
	private void createNewOrganism(Point position, OrganismType organismType)
	{
		Organism organism;

		if (organismType == OrganismType.Wolf)
			organism = new Wolf(this);
		else if (organismType == OrganismType.Sheep)
			organism = new Sheep(this);
		else if (organismType == OrganismType.Fox)
			organism = new Fox(this);
		else if (organismType == OrganismType.Mosquito)
			organism = new Mosquito(this);
		else if (organismType == OrganismType.Llama)
			organism = new Llama(this);
		else if (organismType == OrganismType.Grass)
			organism = new Grass(this);
		else if (organismType == OrganismType.Guarana)
			organism = new Guarana(this);
		else // if (organismType == OrganismType.Thorn)
			organism = new Thorn(this);

		placeNewOrganism(position, organism);
	}

	private static final String saveFileName = "save.csv";
	private int turnCount = 0;

	public static final int boardSize = 20;

	/*
	 * reduce search complexity by providing 2 data structures - for searching in
	 * both directions some organisms will ask for adjacent position neighbours,
	 * some will ask for their own position a more correct (and probably robust)
	 * implementation would use BSP-tree, KD-tree or Octree
	 */
	// hash map for fast object => position search
	HashMap<Organism, Point> map = new HashMap<>();
	// 2D array for fast position => object search
	ArrayList<ArrayList<Organism>> board = new ArrayList<ArrayList<Organism>>();
}
