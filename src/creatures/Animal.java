package creatures;

import java.awt.Point;

import core.World;

public abstract class Animal extends Organism
{
	public Animal(World world, int strength, int initiative)
	{
		super(world, strength, initiative);
	}

	@Override
	public void action(Point myPosition)
	{
		Point newPosition = world.generateAdjacentPosition(myPosition);

		if (world.isFree(newPosition))
			world.moveOrganism(myPosition, newPosition);
		else
			world.getOrganism(newPosition).collideWith(this, myPosition, newPosition);

	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}
}
