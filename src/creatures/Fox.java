package creatures;

import java.awt.Color;
import java.awt.Point;

import core.World;

public class Fox extends Animal
{
	public Fox(World world)
	{
		super(world, 3, 7);
	}

	@Override
	public void action(Point currentPosition)
	{
		Point newPosition = world.generateAdjacentPosition(currentPosition);

		if (world.isFree(newPosition))
		{
			world.moveOrganism(currentPosition, newPosition);
		}
		else
		{
			// fox does not attack stronger organisms
			Organism opponent = world.getOrganism(newPosition);

			if (opponent.getStrength() <= strength)
				opponent.collideWith(this, currentPosition, newPosition);
		}
	}

	@Override
	public void collision(Fox fox, Point myPosition, Point opponentPosition)
	{
		world.placeNewOrganismFromTwoParents(myPosition, opponentPosition, new Fox(world));
	}

	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}

	@Override
	public Color getColor()
	{
		return Color.orange;
	}

	@Override
	public String getTypeName()
	{		
		return OrganismType.Fox.toString();
	}
}