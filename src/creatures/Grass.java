package creatures;

import java.awt.Color;
import java.awt.Point;

import core.World;

public class Grass extends Plant
{
	public Grass(World world)
	{
		super(world, 0);
	}

	@Override
	public void replicate(Point currentPosition)
	{
		world.placeNewOrganismFromOneParent(currentPosition, new Grass(world));
	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}

	@Override
	public Color getColor()
	{
		return Color.green;
	}

	@Override
	public String getTypeName()
	{
		return OrganismType.Grass.toString();
	}
}
