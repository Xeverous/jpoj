package creatures;

import java.awt.Color;
import java.awt.Point;

import core.World;

public class Guarana extends Plant
{
	public Guarana(World world)
	{
		super(world, 0);
	}

	@Override
	public void replicate(Point currentPosition)
	{
		world.placeNewOrganismFromOneParent(currentPosition, new Guarana(world));
	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		organismToBuff = opponent;
		opponent.collision(this, opponentPosition, myPosition);
	}
	
	@Override
	public Color getColor()
	{
		return new Color(0x0000B000);
	}
	
	@Override
	public void die()
	{
		if (organismToBuff != null)
			organismToBuff.modifyStrength(+3);

		super.die();
	}

	@Override
	public String getTypeName()
	{
		return OrganismType.Guarana.toString();
	}

	private Organism organismToBuff = null;
}
