package creatures;

import java.awt.Color;
import java.awt.Point;

import core.World;

public class Llama extends Animal
{
	public Llama(World world)
	{
		super(world, 4, 3);
	}

	@Override
	public Color getColor()
	{
		return Color.yellow;
	}
	
	@Override
	public void collision(Llama llama, Point myPosition, Point opponentPosition)
	{
		world.placeNewOrganismFromTwoParents(myPosition, opponentPosition, new Llama(world));
	}

	@Override
	public void collision(Plant plant, Point myPosition, Point opponentPosition)
	{
		super.collision(plant, myPosition, opponentPosition);

		if (isAlive() && plant.isDead())
			++strength;
	}

	@Override
	public void collision(Thorn thorn, Point myPosition, Point opponentPosition)
	{
		super.collision(thorn, myPosition, opponentPosition);
	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}
	
	@Override
	public String getTypeName()
	{
		return OrganismType.Llama.toString();
	}
}
