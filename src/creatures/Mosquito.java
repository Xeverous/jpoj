package creatures;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import core.World;

public class Mosquito extends Animal
{
	public Mosquito(World world)
	{
		super(world, 1, 1);
	}

	@Override
	public Color getColor()
	{
		return Color.black;
	}
	
	@Override
	public void collision(Mosquito mosquito, Point myPosition, Point opponentPosition)
	{
		world.placeNewOrganismFromTwoParents(myPosition, opponentPosition, new Mosquito(world));
	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}

	@Override
	public void die()
	{
		if (ThreadLocalRandom.current().nextInt(2) == 0)
			super.die();
	}
	
	@Override
	public int getStrength()
	{
		return strength + countAdjacentMosquitos();
	}

	@Override
	public int getInitiative()
	{
		return initiative + countAdjacentMosquitos();
	}
	
	private int countAdjacentMosquitos()
	{
		Point currentPosition = world.getPosition(this);
		
		ArrayList<Point> adjacentPositions = new ArrayList<>();
		world.appendAdjacentPositions(currentPosition, adjacentPositions);
		
		int result = 0;
		
		for (Point position : adjacentPositions)
		{
			Organism organism = world.getOrganism(position);
			
			if (organism == null)
				continue;
			
			if (organism.isDead())
				continue;
			
			if (organism instanceof Mosquito)
				++result;
		}
		
		return result;
	}
	
	@Override
	public String getTypeName()
	{
		return OrganismType.Mosquito.toString();
	}
}
