package creatures;

import java.awt.Color;
import java.awt.Point;
import java.io.FileWriter;
import java.io.IOException;
import core.World;

public abstract class Organism
{
	public Organism(World world, int strength, int initiative)
	{
		this.world = world;
		this.strength = strength;
		this.initiative = initiative;
		this.age = 0;
	}

	public int getStrength()
	{
		return strength;
	}

	public int getInitiative()
	{
		return initiative;
	}

	public int getAge()
	{
		return age;
	}

	public void setStrength(int strength)
	{
		this.strength = strength;
	}

	public void setInitiative(int initiative)
	{
		this.initiative = initiative;
	}

	public void modifyStrength(int difference)
	{
		this.strength += difference;
	}
	public void modifyInitiative(int difference)
	{
		this.initiative += difference;
	}
	public void setAge(int age)
	{
		this.age = age;
	}

	public void doAge() throws Exception
	{
		if (age == deadAge)
			throw new Exception("Dead organisms can't age");

		++age;
	}

	public void die()
	{
		this.age = deadAge;
	}

	public boolean isDead()
	{
		return this.age == deadAge;
	}

	public boolean isAlive() // to avoid double negatives in if (!isDead())
	{
		return !isDead();
	}

	public static final int deadAge = -1;
	public static final char valueDelimeter = ';';
	public static final String emptyFieldToken = "@";
	public static final int tokensPerLine = 4;

	public abstract void action(Point myPosition);

	/**
	 * @defgroup actual colision functions for each type of organism
	 *
	 * by default all call base class case (unless overriden)
	 *
	 * Note 1: templates can not be virtual so there is explicitly written overload for each type
	 * Note 2: place using BaseClass::collision; in each derived class to inform overload resolution to search also in base classes,
	 * otherwise these functions will be name-shadowed and not participate in the list of candidates to call
	 *
	 * @{
	 *
	 * @brief default collision behaviour - fight: kill or be killed
	 */
	public void collision(Organism organism, Point myPosition, Point opponentPosition)
	{
		if (strength >= organism.getStrength())
		{
			organism.die();

			if (organism.isDead()) // some creatures refuse to die
				world.moveOrganism(myPosition, opponentPosition);
		}
		else
		{
			die();
		}
	}

	public void collision(Animal animal,     Point myPosition, Point opponentPosition)
	{
		collision((Organism) animal, myPosition, opponentPosition);
	}

	public void collision(Sheep sheep,       Point myPosition, Point opponentPosition)
	{
		collision((Animal) sheep, myPosition, opponentPosition);
	}

	public void collision(Wolf wolf,         Point myPosition, Point opponentPosition)
	{
		collision((Animal) wolf, myPosition, opponentPosition);
	}

	public void collision(Fox fox,           Point myPosition, Point opponentPosition)
	{
		collision((Animal) fox, myPosition, opponentPosition);
	}

	public void collision(Mosquito mosquito, Point myPosition, Point opponentPosition)
	{
		collision((Animal) mosquito, myPosition, opponentPosition);
	}

	public void collision(Llama llama,       Point myPosition, Point opponentPosition)
	{
		collision((Animal) llama, myPosition, opponentPosition);
	}

	public void collision(Plant plant,       Point myPosition, Point opponentPosition)
	{
		collision((Organism) plant, myPosition, opponentPosition);
	}

	public void collision(Grass grass,       Point myPosition, Point opponentPosition)
	{
		collision((Plant) grass, myPosition, opponentPosition);
	}

	public void collision(Guarana guarana,   Point myPosition, Point opponentPosition)
	{
		collision((Plant) guarana, myPosition, opponentPosition);
	}

	public void collision(Thorn thorn,       Point myPosition, Point opponentPosition)
	{
		collision((Plant) thorn, myPosition, opponentPosition);
	}

	/** @} */

	/**
	 * @brief visitor function dispatching callers to their actual types
	 * @details each derived class must overide this to be able to be dispatched to it's actual type
	 * in each derived class implementation simply write
	 * opponent.collision(*this, opponentPosition, myPosition);
	 * an appropriate overload of collision() will be selected because type of *this is always known exactly
	 */
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, myPosition, opponentPosition);
	}

	public abstract Color getColor();
	public abstract String getTypeName();

	public void save(FileWriter fw) throws IOException
	{
		fw.write(getTypeName());
		fw.write(valueDelimeter);
		writeBaseStats(fw);
		fw.write('\n');
	}

	protected void writeBaseStats(FileWriter fw) throws IOException
	{
		fw.write(String.valueOf(strength));
		fw.write(valueDelimeter);
		fw.write(String.valueOf(initiative));
		fw.write(valueDelimeter);
		fw.write(String.valueOf(age));
		fw.write(valueDelimeter);
	}

	protected World world;
	protected int strength;
	protected int initiative;
	protected int age;
}
