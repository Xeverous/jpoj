package creatures;

public enum OrganismType
{
	Wolf,
	Sheep,
	Fox,
	Mosquito,
	Llama,
	Grass,
	Guarana,
	Thorn
}
