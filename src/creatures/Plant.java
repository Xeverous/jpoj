package creatures;

import java.awt.Point;
import java.util.concurrent.ThreadLocalRandom;

import core.World;

public abstract class Plant extends Organism
{
	public Plant(World world, int strength)
	{
		super(world, strength, 0);
	}

	@Override
	public void action(Point myPosition)
	{
		if (rollSeed())
			replicate(myPosition);
	}
	
	public abstract void replicate(Point currentPosition);
	
	static final double replicationChance = 0.3;
	
	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}
	
	protected boolean rollSeed()
	{
		return ThreadLocalRandom.current().nextInt(0, 2) == 0;
	}
}
