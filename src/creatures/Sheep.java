package creatures;

import java.awt.Color;
import java.awt.Point;

import core.World;

public class Sheep extends Animal
{
	public Sheep(World world)
	{
		super(world, 4, 4);
	}

	@Override
	public Color getColor()
	{
		return Color.darkGray;
	}
	
	@Override
	public void collision(Sheep sheep, Point myPosition, Point opponentPosition)
	{
		world.placeNewOrganismFromTwoParents(myPosition, opponentPosition, new Sheep(world));
	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}

	@Override
	public String getTypeName()
	{
		return OrganismType.Sheep.toString();
	}
}
