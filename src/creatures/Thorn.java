package creatures;

import java.awt.Color;
import java.awt.Point;

import core.World;

public class Thorn extends Plant
{
	public Thorn(World world)
	{
		super(world, 2);
	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}
	
	@Override
	public void replicate(Point currentPosition)
	{
		world.placeNewOrganismFromOneParent(currentPosition, new Thorn(world));
	}

	@Override
	public Color getColor()
	{
		return Color.red;
	}
	
	@Override
	public boolean rollSeed()
	{
		return true;
	}

	@Override
	public String getTypeName()
	{
		return OrganismType.Thorn.toString();
	}
}
