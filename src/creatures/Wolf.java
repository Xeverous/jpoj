package creatures;

import java.awt.Color;
import java.awt.Point;

import core.World;

public class Wolf extends Animal
{
	public Wolf(World world)
	{
		super(world, 9, 5);
	}

	@Override
	public void collision(Wolf wolf, Point myPosition, Point opponentPosition)
	{
		world.placeNewOrganismFromTwoParents(myPosition, opponentPosition, new Wolf(world));
	}

	@Override
	public void collideWith(Organism opponent, Point opponentPosition, Point myPosition)
	{
		opponent.collision(this, opponentPosition, myPosition);
	}
	
	@Override
	public Color getColor()
	{
		return Color.lightGray;
	}

	@Override
	public String getTypeName()
	{
		return OrganismType.Wolf.toString();
	}
}
