package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.*;

@SuppressWarnings("serial")
public class ColorGrid extends JPanel
{
	private JLabel[][] labels;

	public ColorGrid(int rows, int cols, int cellWidth)
	{
		labels = new JLabel[rows][cols];
		Dimension labelPrefSize = new Dimension(cellWidth, cellWidth);
		setLayout(new GridLayout(rows, cols));
		for (int row = 0; row < labels.length; ++row)
		{
			for (int col = 0; col < labels[row].length; ++col)
			{
				JLabel label = new JLabel();
				label = new JLabel();
				label.setOpaque(true);
				label.setBackground(Color.white);
				label.setPreferredSize(labelPrefSize);
				add(label);
				labels[row][col] = label;
			}
		}
	}

	public void setColor(int x, int y, Color color)
	{
		labels[y][x].setBackground(color);
	}
}