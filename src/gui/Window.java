package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import core.World;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class Window
{
	private JFrame frame;
	private World world;

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					new Window();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public Window()
	{
		world = new World();
		createAndShowGui();
	}

	private void createAndShowGui()
	{
		int rows = World.getBoardSize().x;
		int cols = World.getBoardSize().y;
		int cellWidth = 40;
		ColorGrid mainPanel = new ColorGrid(rows, cols, cellWidth);

		frame = new JFrame("Color Grid Example");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);

		JPanel menu = new JPanel();
		frame.getContentPane().add(menu, BorderLayout.SOUTH);

		JButton btnGenerate = new JButton("generuj");
		btnGenerate.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent mouseEvent)
			{
				try
				{
					world.generateRandomWorld();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}

				world.drawOn(mainPanel);
			}
		});
		menu.add(btnGenerate);

		JButton btnNewTurn = new JButton("nowa tura");
		btnNewTurn.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent mouseEvent)
			{
				try
				{
					world.processTurn();
					world.drawOn(mainPanel);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
		menu.add(btnNewTurn);

		JButton btnSave = new JButton("zapisz");
		btnSave.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent mouseEvent)
			{
				try
				{
					world.save();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
		menu.add(btnSave);

		JButton btnLoad = new JButton("wczytaj");
		btnLoad.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent mouseEvent)
			{
				try
				{
					world.load();
					world.drawOn(mainPanel);
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		});
		menu.add(btnLoad);

		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
}
